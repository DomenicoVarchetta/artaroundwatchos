//
//  LocationManager.swift
//  Maps2 WatchKit Extension
//
//  Created by Luigi Mazzarella on 16/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class LocationManager: CLLocationManager{
	private let manager: CLLocationManager
	
	
	init(manager: CLLocationManager = CLLocationManager()) {
		self.manager = manager
		super.init()
	}
	
	
	
	func startUpdating() {
		self.manager.delegate = self
		self.manager.requestWhenInUseAuthorization()
		self.manager.startUpdatingLocation()

	}
	
	
}

extension LocationManager: CLLocationManagerDelegate {
	
	private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if status == .authorizedWhenInUse {
			manager.requestLocation()
		}
	}
	
	private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if locations.first != nil {
			print("location:: (location)")
		}
	}
	
	private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
		print("error:: (error)")
	}
	
	
}
