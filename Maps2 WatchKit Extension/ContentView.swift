//
//  ContentView.swift
//  Maps2 WatchKit Extension
//
//  Created by Luigi Mazzarella on 15/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import SwiftUI
import MapKit


struct ContentView: View {
	
	var body: some View {
		VStack {
			Text("Press Start to Run the app!")
			NavigationLink(destination: MapButton()){
				Text("Localize")
			}
		}
	}
	
}

struct MapButton: WKInterfaceObjectRepresentable {
	

	
	func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<MapButton>) -> WKInterfaceMap {
		
		return WKInterfaceMap()
	}
	
	
	func updateWKInterfaceObject(_ map: WKInterfaceMap, context: WKInterfaceObjectRepresentableContext<MapButton
		>) {
		let locationManager = LocationManager()
		
		repeat{
			locationManager.startUpdating()
			
			print("cordinate: \(String(describing: locationManager.location?.coordinate))")
			let span = MKCoordinateSpan(latitudeDelta: 0.005,
										longitudeDelta: 0.005
			)
			let region = MKCoordinateRegion(
				center: locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)  ,
				span: span)
			
			map.setRegion(region)
			
		}while(locationManager.location?.coordinate == nil )
	}
	
}

struct WatchMapView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
		
	}
}



