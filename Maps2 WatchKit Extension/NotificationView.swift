//
//  NotificationView.swift
//  Maps2 WatchKit Extension
//
//  Created by Luigi Mazzarella on 15/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
