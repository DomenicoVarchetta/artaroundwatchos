//
//  HostingController.swift
//  Maps2 WatchKit Extension
//
//  Created by Luigi Mazzarella on 15/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
